(define-module (pkill9 packages kdenlive-fixed)
  #:use-module (guix download)
  #:use-module (guix build-system trivial)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages))

(define-public kdenlive-fixed
  (package
   (name "kdenlive-fixed")
   (version (package-version (@ (gnu packages kde) kdenlive)))
   (source #f)
   (build-system trivial-build-system)
   (inputs
    `(("kdenlive" ,(@ (gnu packages kde) kdenlive))))
   (propagated-inputs
    `(("mlt" ,(@ (gnu packages video) mlt))))
   (arguments
    `(#:modules ((guix build utils))
      #:builder
      (begin
        (use-modules (guix build utils))
        (let ((out (assoc-ref %outputs "out"))
              (kdenlive (assoc-ref %build-inputs "kdenlive"))
              (mltpath (string-append (assoc-ref %build-inputs "mlt") "/share/mlt/profiles")))
          (install-file (string-append kdenlive "/bin/kdenlive")
                     (string-append out "/bin/"))
          (substitute* (string-append out "/bin/kdenlive")
                       (("\\$@") (string-append "--mlt-path\" \"" mltpath "\" \"$@")))
          (install-file (string-append kdenlive "/share/applications/org.kde.kdenlive.desktop")
                        (string-append out "/share/applications"))
          (substitute* (string-append out "/share/applications/org.kde.kdenlive.desktop")
                       (("Exec=.*") (string-append "Exec=" out "/bin/kdenlive\n")))
          (copy-recursively (string-append kdenlive "/share/icons")
                            (string-append out "/share/icons"))
          ))))
    (home-page "")
    (synopsis "Kdenlive wrapped with mlt-path specified")
    (description synopsis)
    (license #f)))

  (package (inherit kdenlive-fixed))
