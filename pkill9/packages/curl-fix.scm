(define-module (pkill9 packages curl-fix)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (gnu packages irc)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages mpd)
  #:use-module (gnu packages games)
  #:use-module (srfi srfi-1))

(define-public curl-with-ca-path
  (package (inherit (package-replacement curl))
           (name "curl-with-ca-path")
           (arguments
            `(,@(substitute-keyword-arguments (package-arguments (package-replacement curl))
                                              ((#:configure-flags cf)
                                               `(cons "--with-ca-path=/etc/ssl/certs" ,cf))
                                              ((#:phases phases) ;; Disable tests to save time. Can't seem to do so using argument.
                                               `(modify-phases ,phases
                                                               (delete 'check))))))))

(define (package-with-curl-ca-path original-package)
  (package (inherit original-package)
           (name (string-append (package-name original-package) "-with-curl-ca-path"))
           (inputs
            `(,@(alist-delete "curl" (package-inputs original-package))
              ("curl" ,curl-with-ca-path)))))

(define-public weechat-with-curl-ca-path
  (package-with-curl-ca-path weechat))

(define-public minetest-with-curl-ca-path
  (package-with-curl-ca-path minetest))

(define-public mpd-with-curl-ca-path
  (package-with-curl-ca-path mpd))

minetest-with-curl-ca-path
