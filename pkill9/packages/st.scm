(define-module (pkill9 packages st)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (gnu packages suckless)
 )

;(define-public st-with-patches
;  (package (inherit st)
;    (source
;     (origin
;       (method url-fetch)
;       (uri (string-append "https://dl.suckless.org/st/st-"
;                           (package-version st) ".tar.gz"))
;       (sha256
;        (base32
;         "00309qiw20rc89696pk8bdr7ik4r1aarik7jxqk8k66cdj80v1zp"))
;       (patches (search-patches "st-clipboard-0.7.diff"))))
;    ))

(define-public st-with-patches
  (package (inherit st)
    (source
     (origin (inherit (package-source st))
       (patches (search-patches "st-clipboard-0.8.1.diff"))))
    ))
