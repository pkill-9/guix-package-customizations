(define-module (pkill9 packages calibre)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (guix download)
  #:use-module (gnu packages ebook)
 )


(define-public calibre-with-desktop-integration
  (package (inherit calibre)
    (arguments (substitute-keyword-arguments (package-arguments calibre)
     ((#:phases phases)
       `(modify-phases ,phases
         (add-after 'install 'add-desktop-integration
           (lambda _
             (install-file
              "imgsrc/calibre.svg"
              (string-append (assoc-ref %outputs "out")
                             "/share/icons/hicolor/scalable/apps"))
             (mkdir-p (string-append (assoc-ref %outputs "out") "/share/applications"))
             (with-output-to-file
              (string-append (assoc-ref %outputs "out") "/share/applications/calibre.desktop")
            (lambda _
              (format #t
                      "[Desktop Entry]~@
                Name=Calibre~@
                Comment=E-book organiser and reader~@
                Exec=~a/bin/calibre~@
                TryExec=~@*~a/bin/calibre~@
                Icon=calibre~@
                Categories=Office~@
                Type=Application~%"
                      (assoc-ref %outputs "out"))))
             ))))))
    ))
